/*
!-------------------------------------------------------------------------!
!            P R O G R A M M I N G     B A S E L I N E S                  !
!                                                                         !
!                                 F O R                                   !
!                                                                         !
!      N  A  S     P A R A L L E L     B E N C H M A R K S   P B N 1.0    !
!                                                                         !
!                               F T B a s e                               !
!                                                                         !
!-------------------------------------------------------------------------!
!                                                                         !
!    FTBase implements base class for FT benchmark.                       !
!                                                                         !
!    Permission to use, copy, distribute and modify this software         !
!    for any purpose with or without fee is hereby granted.               !
!    We request, however, that all derived work reference the             !
!    NAS Grid Benchmarks 1.0. This software is provided                   !
!    "as is" without express or implied warranty.                         ! 
!                                                                         !
!    Information on NPB 3.0, including the Technical Report NAS-02-008	  !
!    "Implementation of the NAS Parallel Benchmarks in Java",		  !
!    original specifications, source code, results and information        !
!    on how to submit new results, is available at:                       !
!                                                                         !
!         http://www.nas.nasa.gov/Software/NPB/                           !
!                                                                         !
!    Send comments or suggestions to  npb@nas.nasa.gov                    !
!                                                                         !
!          E-mail:  npb@nas.nasa.gov                                      !
!          Fax:     (650) 604-3957                                        !
!                                                                         !
!-------------------------------------------------------------------------!
! Authors: M. Frumkin           					  !
!          M. Schultz           					  !
! Translation to X10:  Tiago Cogumbreiro                                  !
!-------------------------------------------------------------------------!
*/
package NPB3_0_X10.FTThreads;
import NPB3_0_X10.*;
import NPB3_0_X10.FT;

public class FTBase {
    
  public static val BMName="FT";
  
  public var CLASS:Char = 'S';
  
  protected var nx:Int, ny:Int, nz:Int, maxdim:Int, niter_default:Int;

  //complex arrays
  protected var scr:Array[Double];
  protected var plane:Array[Double];
  protected var isize2:Int;
  protected var isize3:Int, jsize3:Int, ksize3:Int;
  protected var isize4:Int, jsize4:Int, ksize4:Int;

  protected var checksum:Array[Double];
  protected var xtr:Array[Double];  //isize3=2;jsize3=2*(ny+1);ksize3=2*(ny+1)*nx;
  protected var xnt:Array[Double];  //isize4=2;jsize4=2*(ny+1);ksize4=2*(ny+1)*nz;
  protected var exp1:Array[Double],exp2:Array[Double],exp3:Array[Double]; 

  public var timeron:Boolean=false;
  public val timer = new Timer();

  //constants
  protected static val REAL=0,IMAG=1;
  protected static val pi=Math.PI,alpha=.000001;

  public def this(){}
  
  public def this(clss:Char, np:Int, serial:Boolean){
    CLASS = clss;
    num_threads=np;
    switch (CLASS){
    case 'S':
      nx=ny=nz=64;
      niter_default=6;
      break;
    case 'W':
      nx=ny=128;
      nz=32;
      niter_default=6;
      break;     
    case 'A':
      nx=256;
      ny=256;
      nz=128;
      niter_default=6;
      break;      
    case 'B':
      nx=512;
      ny=nz=256;
      niter_default=20;
      break;
    case 'C':
      nx=ny=nz=512;
      niter_default=20;      
      break;
    }    
    maxdim = max( nx , max( ny , nx ) );
    if(serial){
      scr = new Array[Double](2*(maxdim+1)*maxdim);
      plane = new Array[Double](2*(maxdim+1)*maxdim);
    }
    isize2=2;
    isize3=2;
    jsize3=2*(ny+1);
    ksize3=2*(ny+1)*nx;
    isize4=2;
    jsize4=2*(ny+1);
    ksize4=2*(ny+1)*nz;
    //complex values
    checksum = new Array[Double](2*niter_default); //isize2=2;

    xtr = new Array[Double](2*(ny+1)*nx*nz); 
    xnt = new Array[Double](2*(ny+1)*nz*nx); 
    exp1 = new Array[Double](2*nx);
    exp2 = new Array[Double](2*ny);
    exp3 = new Array[Double](2*nz);
  }

  // thread variables
  protected var master:FT;
  protected var num_threads:Int;
  protected var t_doFFT:Array[FFTThread];
  protected var t_doEvolve:Array[EvolveThread];

  public def setupThreads(ft:FT){
    master = ft;
    if(num_threads>nz) num_threads=nz;
    if(num_threads>nx) num_threads=nx;
    
    val interval1=new Array[Int](num_threads);
    val interval2=new Array[Int](num_threads);
    set_interval(num_threads,nz,interval1);
    set_interval(num_threads,nx,interval2);

    var partition1:Array[Array[Int]] = Array_2.make[Int](interval1.size, 2);
    var partition2:Array[Array[Int]] = Array_2.make[Int](interval2.size, 2);
    partition1 = Array_2.make[Int](interval1.size, 2);
    partition2 = Array_2.make[Int](interval2.size, 2);
    set_partition(0,interval1,partition1);
    set_partition(0,interval2,partition2);
    t_doFFT = new Array[FFTThread](num_threads);    
    t_doEvolve = new Array[EvolveThread](num_threads);

    for(var ii:Int=0;ii<num_threads;ii++){
      t_doFFT(ii) = new FFTThread(ft,partition1(ii)(0),partition1(ii)(1),
				   partition2(ii)(0),partition2(ii)(1));
      t_doFFT(ii).id=ii;
      val fft = t_doFFT(ii); async clocked(ft.c_doFFT) fft.run();

      t_doEvolve(ii) = new EvolveThread(ft,partition2(ii)(0),partition2(ii)(1));
      t_doEvolve(ii).id=ii;
      val ev = t_doEvolve(ii); async clocked(ft.c_doEvolve) ev.run();
    }
  }
  public def set_interval(threads:Int, problem_size:Int, interval:Array[Int] ){
    interval(0)= problem_size/threads;
    for(var i:Int=1;i<threads;i++) interval(i)=interval(0);
    val remainder = problem_size%threads;
    for(var i:Int=0;i<remainder;i++) interval(i)+=1;
  }
  
  public def set_partition(start:Int, interval:Array[Int], prt:Array[Array[Int]]){
    prt(0)(0)=start;
    if(start==0) prt(0)(1)=interval(0)-1;
    else prt(0)(1)=interval(0);
    
    for(var i:Int=1;i<interval.size;i++){
      prt(i)(0)=prt(i-1)(1)+1;
      prt(i)(1)=prt(i-1)(1)+interval(i);
    }
  }
  
  public static def max(a:Int, b:Int){if(a>b)return a; else return b;}

  public def CompExp (n:Int, exponent:Array[Double] ){     
    val nu = n;
    val m = ilog2(n);
    exponent(0) = m;
    
    val eps=1.0E-16;
    var ku:Int = 1;
    var ln:Int = 1;
    for(var j:Int=1;j<=m;j++){
      val t = pi / ln;
      for(var i:Int=0;i<=ln-1;i++){
	val ti = i * t;
	val idx=(i+ku)*2;
	exponent(REAL+idx) = Math.cos(ti);
	exponent(IMAG+idx) = Math.sin(ti);
	if(Math.abs(exponent(REAL+idx)) < eps) exponent(REAL+idx)=0;
	if(Math.abs(exponent(IMAG+idx)) < eps) exponent(IMAG+idx)=0;
      }
      ku = ku + ln;
      ln = 2 * ln;
    }
  }
  
  public def initial_conditions(u0:Array[Double], 
                                d1:Int, d2:Int, d3:Int){
    val tmp= new Array[Double](2*maxdim);
    val RanStarts= new Array[Double](maxdim);
    			//seed has to be init here since
    			//is called 2 times 
    var seed:Double = 314159265., a:Double=Math.pow(5.0,13);
    var start:Double = seed;
//---------------------------------------------------------------------
// Jump to the starting element for our first plane.
//---------------------------------------------------------------------
    val rng=new Random(seed);
    var an:Double = rng.ipow46(a, 0);
    rng.randlc(seed, an);
    an = rng.ipow46(a, 2*d1*d2);
//---------------------------------------------------------------------
// Go through by z planes filling in one square at a time.
//---------------------------------------------------------------------
    RanStarts(0) = start;
    for(var k:Int=1;k<d3;k++){
      seed = rng.randlc(start, an);
      RanStarts(k)=start=seed;
    }
    for(var k:Int=0;k<d3;k++){
      var x0:Double = RanStarts(k);
      for(var j:Int=0;j<d1;j++){
        x0 = rng.vranlc(2*d2, x0, a, tmp,0);
        for(var i:Int=0;i<d2;i++){
          u0(REAL+j*isize3+i*jsize3+k*ksize3)=tmp(REAL+i*2);
          u0(IMAG+j*isize3+i*jsize3+k*ksize3)=tmp(IMAG+i*2);
        }
      }
    }
  }

  public def ilog2(n:Int){
    var nn:Int, lg:Int;
    if (n == 1) return 0;
    lg = 1;
    nn = 2;
    while (nn < n){
      nn = nn*2;
      lg = lg+1;
    }
   return lg;
  }

  protected /*static*/ var fftblock_default:Int=4*4096, //Size of L1 cache on SGI O2K
                       fftblock:Int=0;
  public def Swarztrauber(is:Int, m:Int, len:Int, n:Int, x:Array[Double], xoffst:Int,
			   xd1:Int, exponent:Array[Double], scr:Array[Double]){
    var i:Int;var j:Int=0;var l:Int,mx:Int;
    var k:Int, n1:Int,li:Int,lj:Int,lk:Int,ku:Int,i11:Int,i12:Int,i21:Int,i22:Int;
    var BlockStart:Int,BlockEnd:Int;
    var isize1:Int=2,jsize1:Int=2*(xd1+1);
    
    //complex values
    val u1 = new Array[Double](2), x11 = new Array[Double](2),x21 = new Array[Double](2);
    if(timeron) timer.start(4);
    
//---------------------------------------------------------------------
//   Perform one variant of the Stockham FFT.
//---------------------------------------------------------------------

    fftblock = fftblock_default/n;
    if (fftblock<8) fftblock=8;
    for(BlockStart=0; BlockStart<len;BlockStart+=fftblock){
      BlockEnd = BlockStart + fftblock - 1;
      if ( BlockEnd >= len) BlockEnd = len-1;
      for(l=1;l<=m;l+=2){
	n1 = n / 2;
	lk = Math.pow(2, l-1) as Int;
	li = Math.pow(2, m-l) as Int;
	lj = 2 * lk;
	ku = li;
	
	for(i=0;i<=li-1;i++){
	  i11 = i * lk;
	  i12 = i11 + n1;
	  i21 = i * lj;
	  i22 = i21 + lk;
	  
	  u1(REAL)= exponent(REAL + (ku+i)*2);
	  if (is >= 1){  
	    u1(IMAG)= exponent(IMAG + (ku+i)*2);
	  }else{
	    u1(IMAG) = - exponent(IMAG + (ku+i)*2);
	  }
	  for(k=0;k<=lk-1;k++){
	    for(j=BlockStart;j<=BlockEnd;j++){	      
	      x11(REAL) = x(REAL+j*2+(i11+k)*jsize1 + xoffst);
	      x11(IMAG) = x(IMAG+j*2+(i11+k)*jsize1 + xoffst);
	      x21(REAL) = x(REAL+j*2+(i12+k)*jsize1 + xoffst);
	      x21(IMAG) = x(IMAG+j*2+(i12+k)*jsize1 + xoffst);
	      scr(REAL+j*isize1+(i21+k)*jsize1) = x11(REAL) + x21(REAL);
	      scr(IMAG+j*isize1+(i21+k)*jsize1) = x11(IMAG) + x21(IMAG);
	      scr(REAL+j*2+(i22+k)*jsize1) = u1(REAL) * (x11(REAL) - x21(REAL))
	                                   - u1(IMAG) * (x11(IMAG) - x21(IMAG));
	      scr(IMAG+j*2+(i22+k)*jsize1) = u1(IMAG) * (x11(REAL) - x21(REAL))
	                                   + u1(REAL) * (x11(IMAG) - x21(IMAG));
	    }
	  }
	}
	if(l==m){
	  for(k=0;k<n;k++){
	    for(j=BlockStart;j<=BlockEnd;j++){
	      x(REAL+j*2+k*jsize1 + xoffst) = scr(REAL+j*isize1+k*jsize1);
	      x(IMAG+j*2+k*jsize1 + xoffst) = scr(IMAG+j*isize1+k*jsize1);
	    }
	  }
	}else{
	  n1 = n / 2;
	  lk = Math.pow(2,l) as Int;
	  li = Math.pow(2, m - l - 1) as Int;
	  lj = 2 * lk;
	  ku = li;
	  
	  for(i=0;i<=li-1;i++){
	    i11 = i * lk;
	    i12 = i11 + n1;
	    i21 = i * lj;
	    i22 = i21 + lk;
	    
	    u1(REAL) = exponent(REAL+(ku+i)*2);
	    if (is>=1){
	      u1(IMAG) = exponent(IMAG+(ku+i)*2);
	    }else{
	      u1(IMAG) = - exponent(IMAG +(ku+i)*2);
	    }
	    for(k=0;k<=lk-1;k++){
	      for(j=BlockStart;j<=BlockEnd;j++){
		x11(REAL) = scr(REAL+j*isize1+(i11+k)*jsize1);
		x11(IMAG) = scr(IMAG+j*isize1+(i11+k)*jsize1);

		x21(REAL) = scr(REAL+j*isize1+(i12+k)*jsize1);
		x21(IMAG) = scr(IMAG+j*isize1+(i12+k)*jsize1);

		x(REAL+j*2+(i21+k)*jsize1+xoffst) = x11(REAL) + x21(REAL);
		x(IMAG+j*2+(i21+k)*jsize1+xoffst) = x11(IMAG) + x21(IMAG);

		x(REAL+j*2+(i22+k)*jsize1+xoffst) = u1(REAL) * (x11(REAL) - x21(REAL))
		                                  - u1(IMAG) * (x11(IMAG) - x21(IMAG));
		x(IMAG+j*2+(i22+k)*jsize1+xoffst) = u1(IMAG) * (x11(REAL) - x21(REAL))
                                                  + u1(REAL) * (x11(IMAG) - x21(IMAG));
	      }
	    }
	  }
	}
      }
    }    
    if(timeron) timer.stop(4);       
  }
}
