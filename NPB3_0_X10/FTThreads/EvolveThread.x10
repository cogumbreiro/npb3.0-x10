/*
!-------------------------------------------------------------------------!
!									  !
!	 N  A  S     P A R A L L E L	 B E N C H M A R K S  3.0	  !
!									  !
!			J A V A 	V E R S I O N			  !
!									  !
!                        E v o l v e T h r e a d                          !
!                                                                         !
!-------------------------------------------------------------------------!
!                                                                         !
!    EvolveThread implements array evolving thread for FT benchmark.      !
!									  !
!    Permission to use, copy, distribute and modify this software	  !
!    for any purpose with or without fee is hereby granted.  We 	  !
!    request, however, that all derived work reference the NAS  	  !
!    Parallel Benchmarks 3.0. This software is provided "as is" 	  !
!    without express or implied warranty.				  !
!									  !
!    Information on NPB 3.0, including the Technical Report NAS-02-008	  !
!    "Implementation of the NAS Parallel Benchmarks in Java",		  !
!    original specifications, source code, results and information	  !
!    on how to submit new results, is available at:			  !
!									  !
!	    http://www.nas.nasa.gov/Software/NPB/			  !
!									  !
!    Send comments or suggestions to  npb@nas.nasa.gov  		  !
!									  !
!	   NAS Parallel Benchmarks Group				  !
!	   NASA Ames Research Center					  !
!	   Mail Stop: T27A-1						  !
!	   Moffett Field, CA   94035-1000				  !
!									  !
!	   E-mail:  npb@nas.nasa.gov					  !
!	   Fax:     (650) 604-3957					  !
!									  !
!-------------------------------------------------------------------------!
! Translation to Java and MultiThreaded Code	 			  !
!	   M. Frumkin							  !
!	   M. Schultz							  !
! Translation to X10:  Tiago Cogumbreiro                                  !
!-------------------------------------------------------------------------!
*/
package NPB3_0_X10.FTThreads;
import NPB3_0_X10.FT;
import x10.compiler.NonEscaping;

public class EvolveThread extends FTBase{
  public var kt:Int=0;
  public var id:Int;
  public var done:Boolean = true;
  
  var lower_bound1:Int,upper_bound1:Int; 
  var xtr:Array[Double],xnt:Array[Double]; 
  var ixnt:Int,jxnt:Int,kxnt:Int;
  var ixtr:Int,jxtr:Int,kxtr:Int;
  val clock:Clock;
  
  static val ap:Double =  (- 4.0 * alpha * pi*pi );

  public def this(ft:FT, low1:Int, high1:Int){
    Init(ft);
    lower_bound1=low1;
    upper_bound1=high1;
    //XXX:setPriority(Thread.MAX_PRIORITY);
    //XXX:setDaemon(true);
    master = ft;
    clock = ft.c_doEvolve;
  }
  @NonEscaping
  final public def Init(ft:FT){
    //initialize shared data
    xtr=ft.xtr;
    xnt=ft.xnt;
    
    nx=ft.nx;
    ny=ft.ny;
    nz=ft.nz;
    
    ixtr=ft.isize3;
    jxtr=ft.jsize3;
    kxtr=ft.ksize3;
    ixnt=ft.isize4;
    jxnt=ft.jsize4;
    kxnt=ft.ksize4;
  }

  public def run(){    
    while(!master.isDone()){
      clock.advance();if (master.isDone()) break;
      step();
      clock.advance();if (master.isDone()) break;
    }
  }

  public def step(){
    for(var i:Int=lower_bound1;i<=upper_bound1;i++){
      val ii:Int = i-(i/(nx/2))*nx;
      val ii2:Int = ii*ii;
      for(var k:Int=0;k<nz;k++){
	val kk:Int = k-(k/(nz/2))*nz;
	val ik2:Int = ii2 + kk*kk;
	for(var j:Int=0;j<ny;j++){
	  val jj:Int = j-(j/(ny/2))*ny;
	  val lexp:Double=Math.exp((ap*(jj*jj + ik2))*(kt+1));
	  val xntidx:Int=j*ixnt+k*jxnt+i*kxnt;
	  val xtridx:Int=j*ixtr+i*jxtr+k*kxtr;
	  xnt(REAL+xntidx) = lexp*xtr(REAL+xtridx); 
	  xnt(IMAG+xntidx) = lexp*xtr(IMAG+xtridx);
	}
      }
    }   
  }
}
