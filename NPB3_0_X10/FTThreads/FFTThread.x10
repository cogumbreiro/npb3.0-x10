/*
!-------------------------------------------------------------------------!
!									  !
!	 N  A  S     P A R A L L E L	 B E N C H M A R K S  3.0	  !
!									  !
!			J A V A 	V E R S I O N			  !
!									  !
!                           F F T T h r e a d                             !
!                                                                         !
!-------------------------------------------------------------------------!
!                                                                         !
!    FFTThread implements a threaded version of FFT  for FT benchmark.    !
!									  !
!    Permission to use, copy, distribute and modify this software	  !
!    for any purpose with or without fee is hereby granted.  We 	  !
!    request, however, that all derived work reference the NAS  	  !
!    Parallel Benchmarks 3.0. This software is provided "as is" 	  !
!    without express or implied warranty.				  !
!									  !
!    Information on NPB 3.0, including the Technical Report NAS-02-008	  !
!    "Implementation of the NAS Parallel Benchmarks in Java",		  !
!    original specifications, source code, results and information	  !
!    on how to submit new results, is available at:			  !
!									  !
!	    http://www.nas.nasa.gov/Software/NPB/			  !
!									  !
!    Send comments or suggestions to  npb@nas.nasa.gov  		  !
!									  !
!	   NAS Parallel Benchmarks Group				  !
!	   NASA Ames Research Center					  !
!	   Mail Stop: T27A-1						  !
!	   Moffett Field, CA   94035-1000				  !
!									  !
!	   E-mail:  npb@nas.nasa.gov					  !
!	   Fax:     (650) 604-3957					  !
!									  !
!-------------------------------------------------------------------------!
! Translation to Java and MultiThreaded Code	 			  !
!	   M. Frumkin							  !
!	   M. Schultz							  !
! Translation to X10:  Tiago Cogumbreiro                                  !
!-------------------------------------------------------------------------!
*/
package NPB3_0_X10.FTThreads;
import NPB3_0_X10.FT;
import x10.compiler.NonEscaping;

public class FFTThread extends FTBase{
  public var id:Int;
  
  var x:Array[Double];
  var exp1:Array[Double];
  var exp2:Array[Double];
  var exp3:Array[Double];
  var n1:Int,n2:Int,n3:Int;
  var lower_bound1:Int,upper_bound1:Int,lower_bound2:Int,upper_bound2:Int;
  var lb1:Int,ub1:Int,lb2:Int,ub2:Int;
  
  var state:Int;
  var sign:Int;
  var plane:Array[Double];
  var scr:Array[Double];
  val clock:Clock;
  
  public def this(ft:FT, low1:Int, high1:Int, low2:Int, high2:Int){
    Init(ft);
    state=1;
    lb1=low1;
    ub1=high1;
    lb2=low2;
    ub2=high2;   
    plane=new Array[Double](2*(maxdim+1)*maxdim);
    scr = new Array[Double](2*(maxdim+1)*maxdim);
    //XXX:setPriority(Thread.MAX_PRIORITY);
    //XXX:setDaemon(true);
    master = ft;
    clock=ft.c_doFFT;
  }
  @NonEscaping
  final def Init(ft:FT){
    //initialize shared data
    maxdim=ft.maxdim;
  }
  public def run(){
    while(!master.isDone()){
      clock.advance();if (master.isDone()) break;
      step();
      state++;
      if(state==4) state=1;
      clock.advance();if (master.isDone()) break;
    }
  }
  
  public def setVariables(sign1:Int, tr:Boolean, x1:Array[Double], 
                           exp11:Array[Double], exp21:Array[Double], exp31:Array[Double]){
    sign = sign1; 
    x = x1;
    exp1 = exp11;
    exp2 = exp21;
    exp3 = exp31;
    n1=exp1.size>>1;
    n2=exp2.size>>1;
    n3=exp3.size>>1;
    
    if(tr){
      lower_bound1=lb2;
      upper_bound1=ub2;
      lower_bound2=lb1;
      upper_bound2=ub1;
    }else{
      lower_bound1=lb1;
      upper_bound1=ub1;
      lower_bound2=lb2;
      upper_bound2=ub2;   
    }
  }
  
  public def step(){
    switch(state){
    case 1:
      step1();
      break;
    case 2:
      step2();
      break;
    case 3:
      step3();
      break;
    }
  }
  public def step1() {
    val log:Int = ilog2(n2);
    val isize3:Int=2;
    val jsize3:Int=isize3*(n1+1);
    val ksize3:Int=jsize3*n2;
    val isize1:Int=2;
    val jsize1:Int=2*(n2+1);
    for(var k:int=lower_bound1;k<=upper_bound1;k++)
      Swarztrauber(sign,log,n1,n2,x, k*ksize3 ,n1,exp2,scr);
  }
  public def step2() {
    var log:Int = ilog2(n2);
    val isize3:Int=2;
    val jsize3:Int=isize3*(n1+1);
    val ksize3:Int=jsize3*n2;
    val isize1:Int=2;
    val jsize1:Int=2*(n2+1);
    log = ilog2(n1);
    for(var k:Int=lower_bound1;k<=upper_bound1;k++){
      for(var j:Int=0;j<n2;j++){
        for(var i:Int=0; i<n1;i++){
          plane(REAL+j*isize1+i*jsize1) = x(REAL+i*isize3+j*jsize3+k*ksize3);
          plane(IMAG+j*isize1+i*jsize1) = x(IMAG+i*isize3+j*jsize3+k*ksize3);
        }
      }
      Swarztrauber(sign,log,n2,n1,plane,0,n2,exp1,scr);
      for(var j:Int=0;j<n2;j++){
        for(var i:Int=0;i<n1;i++){
          x(REAL+i*isize3+j*jsize3+k*ksize3)=plane(REAL+j*isize1+i*jsize1);
          x(IMAG+i*isize3+j*jsize3+k*ksize3)=plane(IMAG+j*isize1+i*jsize1);
        }
      }
    }
  }
  public def step3() {
    var log:Int = ilog2(n2);
    val isize3:Int=2;
    val jsize3:Int=isize3*(n1+1);
    val ksize3:Int=jsize3*n2;
    val isize1:Int=2;
    var jsize1:Int=2*(n2+1);
    log = ilog2(n3);     
    jsize1=2*(n1+1);
    for(var k:Int=lower_bound2;k<=upper_bound2;k++) {
      for(var i:Int=0; i<n3;i++){
        for(var j:Int=0;j<n1;j++){
            plane(REAL+j*isize1+i*jsize1) = x(REAL+j*isize3+k*jsize3+i*ksize3);
          plane(IMAG+j*isize1+i*jsize1) = x(IMAG+j*isize3+k*jsize3+i*ksize3);
        }
      }
      Swarztrauber(sign,log,n1,n3,plane,0,n1,exp3,scr);
      for(var i:Int=0; i<n3;i++){
        for(var j:Int=0;j<n1;j++){
          x(REAL+j*isize3+k*jsize3+i*ksize3)=plane(REAL+j*isize1+i*jsize1);
          x(IMAG+j*isize3+k*jsize3+i*ksize3)=plane(IMAG+j*isize1+i*jsize1);
        }
      }
    }         
  }
}














