/*
!-------------------------------------------------------------------------!
!									  !
!	 N  A  S     P A R A L L E L	 B E N C H M A R K S  3.0	  !
!									  !
!			J A V A 	V E R S I O N			  !
!									  !
!                           B M R E S A L T S                             !
!                                                                         !
!-------------------------------------------------------------------------!
!                                                                         !
!    BMResults implements Benchmark Result class                          !
!									  !
!    Permission to use, copy, distribute and modify this software	  !
!    for any purpose with or without fee is hereby granted.  We 	  !
!    request, however, that all derived work reference the NAS  	  !
!    Parallel Benchmarks 3.0. This software is provided "as is" 	  !
!    without express or implied warranty.				  !
!									  !
!    Information on NPB 3.0, including the Technical Report NAS-02-008	  !
!    "Implementation of the NAS Parallel Benchmarks in Java",		  !
!    original specifications, source code, results and information	  !
!    on how to submit new results, is available at:			  !
!									  !
!	    http://www.nas.nasa.gov/Software/NPB/			  !
!									  !
!    Send comments or suggestions to  npb@nas.nasa.gov  		  !
!									  !
!	   NAS Parallel Benchmarks Group				  !
!	   NASA Ames Research Center					  !
!	   Mail Stop: T27A-1						  !
!	   Moffett Field, CA   94035-1000				  !
!									  !
!	   E-mail:  npb@nas.nasa.gov					  !
!	   Fax:     (650) 604-3957					  !
!									  !
!-------------------------------------------------------------------------!
!     Translation to Java and to MultiThreaded Code:			  !
!     Michael A. Frumkin					          !
!     Translation to X10:  Tiago Cogumbreiro                              !
!-------------------------------------------------------------------------!
*/
package NPB3_0_X10.BMInOut;
//import java.io.*;
//import java.text.*;
import x10.util.StringBuilder;
import x10.io.Writer;

public class BMResults /*XXX:implements Serializable*/{
  public var name:String;
  public var MachineName:String;
  public var PrLang:String;
  public var clss:Char;
  public var n1:Int,n2:Int,n3:Int,niter:Int;
  public var time:Double,acctime:Double,wctime:Double,mops:Double;
  public var tmSent:Double=0.0,tmReceived:Double=0.0;
  public var RecArrSize:Int=0;
  public var optype:String;
  public var numthreads:Int;
  public var serial:Boolean;
  public var pid:Int;  
  public var verified:Int;
  public var out:Writer=null;
  
  public def this(){}
  
  public def this(bid:Int){
    pid=bid;
    clss='S';
    optype="floating point";
  }

  public def this(bname:String,
		   CLASS:Char,
		   bn1:Int, 
		   bn2:Int,
		   bn3:Int,
		   bniter:Int,
		   btime:Double,
		   bmops:Double,
		   boptype:String,
		   passed_verification:Int,
		   bserial:Boolean,
	       num_threads:Int,
	       bid:Int){ 
      pid=bid;
      name=bname;
      clss=CLASS;
      n1=bn1;
      n2=bn2;
      n3=bn3;
      niter=bniter;
      time=btime;
      mops=bmops;
      optype=boptype;
      verified=passed_verification;
      serial=bserial;
      numthreads=num_threads;
  }
  private static val LINE = "                                "
                            + "                               *";
  private static def format(line:String) {
    var outbuf:StringBuilder=new StringBuilder();
    outbuf.add(LINE);
    outbuf.insert(0,line);
    outbuf.insert(LINE.length()-1,"*");
    return outbuf.result().substring(0, LINE.length());
  }
  
  private static def println(line:String) {
    Console.OUT.println(format(line));
  }
  private static def println(line:String, extra:Any) {
    val args = new Array[Any](1);
    args(0) = extra;
    println(String.format(line, args));
  }
  
  public def print(){    
    println("***** NAS Parallel Benchmarks"+
                              " X10 version (NPB3_0_X10) "+name+" ****");
    println("* Class             = "+clss);
    if( n2 == 0 && n3 == 0 ){
      println("* Size              = "+n1);
    }else{
      println("* Size              = "+n1+" X "+n2+" X "+n3);
    }
    println("* Iterations        = "+niter);
    println("* Time in seconds   = %.3f", time);
    println("* ACCTime           = %.3f", acctime);
    println("* Mops total        = %.3f", mops);
    println("* Operation type    = "+optype);
    if(verified==1)println("* Verification      = Successful");
    else if(verified==0)println("* Verification      = Failed");
    else println("* Verification      = Not Performed");
    
    if(!serial){
      println("* Threads requested = "+numthreads);
    }
    println("*");
    println("* Please send all errors/feedbacks to:");
    println("* NPB Working Team");
    println("* npb@nas.nasa.gov");
    println("********************************"
                       +"*******************************");
    
    if(out!=null){/*XXX:
      try{
 	outline="***** NAS Parallel Benchmarks Java version (NPB3_0_JAV) "
	              +name+" Report *****";
        out.write(outline,0,outline.length());
        out.newLine();
        outline="Class           = "+String.valueOf(clss);
        out.write(outline,0,outline.length());
        out.newLine();
        if( n2 == 0 && n3 == 0 ){
          outline="Size            = "+String.valueOf(n1);
        }else{
          outline="Size            = "+String.valueOf(n1)+" X "+
	                               String.valueOf(n2)+" X "+
				       String.valueOf(n3);
        }
        out.write(outline,0,outline.length());
        out.newLine();
        outline="Iterations      = "+String.valueOf(niter);
        out.write(outline,0,outline.length());
        out.newLine();
        outline="Time in seconds = "+String.valueOf(fmt.format(time));
        out.write(outline,0,outline.length());
        out.newLine();
        outline="ACCTime         = "+String.valueOf(fmt.format(acctime));
        out.write(outline,0,outline.length());
        out.newLine();
        outline="Mops total      = "+String.valueOf(fmt.format(mops));
        out.write(outline,0,outline.length());
        out.newLine();
        outline="Operation type  = "+String.valueOf(optype);
        out.write(outline,0,outline.length());
        out.newLine();
        if(verified==1)      outline="Verification    = Successful";
        else if(verified==0) outline="Verification Failed";
        else                 outline="Verification Not Performed";
        out.write(outline,0,outline.length());
        out.newLine();
    
    	outline="\n Please send all errors/feedbacks to:";
        out.write(outline,0,outline.length());
        out.newLine();
        outline=" NPB Working Team";
        out.write(outline,0,outline.length());
        out.newLine();
        outline=" npb@nas.nasa.gov\n";
        out.write(outline,0,outline.length());
        out.newLine();
	out.flush();
      }catch(Exception e){
        System.err.println("Res.print: write file: "+e.toString());
      }*/
    }    
  }  /*

  public int getFromFile(String filename){
    BufferedReader in=null;
    verified=-1;
    try{
      in = new BufferedReader(new FileReader(filename));
    }catch(Exception e){
      System.err.println("BMResults.getFromFile: filename "+e.toString());   
      return 0;
    }
    String line=new String();
    String keyword;
    int idx1;
    try{
      while((line=in.readLine())!=null){
        if(line.indexOf("Time in seconds =")>=0){
          keyword=new String("Time in seconds =");
          idx1=line.indexOf(keyword);
          idx1+=keyword.length();
          Double dbl=new Double(line.substring(idx1));
          acctime=wctime=dbl.doubleValue();
        }else if(line.indexOf("Verification    =")>=0){
	  verified=0;
          if(  line.indexOf("successful")>=0&&line.indexOf("successful")<0
	     ||line.indexOf("SUCCESSFUL")>=0&&line.indexOf("UNSUCCESSFUL")<0)
	    verified=1;
        }else if(line.indexOf("Mop/s total     =")>=0){
          keyword=new String("Mop/s total     =");
          idx1=line.indexOf(keyword);
          idx1+=keyword.length();
          Double dbl=new Double(line.substring(idx1));
          mops=dbl.doubleValue();
        }
      }
    }catch(Exception e){
      System.err.println("BMResults.getFromFile: "+e.toString());   
      return 0;
    }
//    print();
    return 1;
  }*/
  public static def printVerificationStatus(clss:Char, verified:Int, BMName:String){
    if (clss == 'U'||verified == -1) {
      //XXX:verified = -1;
      Console.OUT.println(" Problem size unknown");
      Console.OUT.println(BMName+"."+clss+": Verification Not Performed");
    }else if (verified==1) {
      Console.OUT.println(BMName+"."+clss+": Verification Successful");
    }else{
      Console.OUT.println(BMName+"."+clss+": Verification Failed");
    }
  }
  public static def printComparisonStatus(clss:Char,var verified:Int, epsilon:Double,
                       xcr:Array[Double],xcrref:Array[Double],xcrdif:Array[Double]):Int{
    for(var m:Int=0;m<xcr.size;m++){
      if (clss == 'U'){
        Console.OUT.println(m+". "+xcr(m));
      }else{
        if   (xcrdif(m) <= epsilon){
          if(verified==-1) verified = 1;
        }else{
          verified = 0;
          Console.OUT.print("FAILURE: ");
        }
        Console.OUT.println(m+". "+xcr(m)+" "+xcrref(m)+" "+xcrdif(m));
      }
    }
    return verified;
  }
  public static def printComparisonStatus(clss:Char, var verified:Int, epsilon:Double,
                       xcr:Double, xcrref:Double, xcrdif:Double):Int{
    if (clss == 'U'){
      Console.OUT.println(" "+xcr);
    }else{
      if(xcrdif <= epsilon){
    	if(verified==-1) verified = 1;
      }else{
    	verified = 0;
    	Console.OUT.print("FAILURE: ");
      }
      Console.OUT.println(xcr+" "+xcrref+" "+xcrdif);
    }
    return verified;
  }
}
