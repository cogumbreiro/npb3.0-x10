X10C = x10c

COMMON_SOURCES = \
	NPB3_0_X10/Array_2.x10 \
	NPB3_0_X10/Random.x10 \
	NPB3_0_X10/BMInOut/BMArgs.x10 \
	NPB3_0_X10/BMInOut/BMResults.x10 \
	NPB3_0_X10/Timer.x10


FT_SOURCES = \
	$(COMMON_SOURCES) \
	NPB3_0_X10/FTThreads/FTBase.x10 \
	NPB3_0_X10/FTThreads/EvolveThread.x10 \
	NPB3_0_X10/FTThreads/FFTThread.x10 \
	NPB3_0_X10/FT.x10

all: ft

bt:
	@echo TODO

cg:
	@echo TODO

is:
	@echo TODO

lu:
	@echo TODO

mg:
	@echo TODO

sp:
	@echo TODO

FT.jar: $(FT_SOURCES)

ft: FT.jar

%.jar: NPB3_0_X10/%.x10
	$(eval TMPDIR := $(shell mktemp -d))
	@test -d $(TMPDIR)
	$(X10C) -d $(TMPDIR) $(X10FLAGS) $<
	find $(TMPDIR) -name '*.java' -exec rm '{}' ';'
	jar cf $@ -C $(TMPDIR) .
	rm -rf $(TMPDIR)

clean:
	rm -f NPB3_0_X10/*.class ??Threads/*.class

